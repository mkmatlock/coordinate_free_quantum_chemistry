FROM ubuntu:18.04
LABEL maintainer="Matt Matlock <matt.matlock@gmail.com>"

# TensorFlow version is tightly coupled to CUDA and cuDNN so it should be selected carefully
ENV PYTHON_VERSION=2
ENV TF_VERSION=1.14.0
ENV KERAS_VERSION=2.2.4
ENV HOROVOD_VERSION=0.18.1
ENV NUMPY_VERSION=1.16.5
ENV SCIPY_VERSION=1.2.1
ENV PANDAS_VERSION=0.23.4
ENV NETWORKX_VERSION=2.1
ENV H5PY_VERSION=2.10.0
ENV PYARROW_VERSION=0.14.1
ENV SONNET_VERSION=1.34
ENV OPENBABEL_VERSION=2-4-1
ENV PIP=pip$PYTHON_VERSION

RUN mkdir -p /tmp && chmod 1777 /tmp
SHELL ["/bin/bash", "-c"]
# Pick up some TF dependencies
RUN apt-get -y update --fix-missing && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        libfreetype6-dev \
        libhdf5-serial-dev \
        libzmq3-dev \
        pkg-config \
        software-properties-common \
        unzip \
        libc6-dbg gdb valgrind \
        cmake \
        git \
        mercurial \
        vim \
        wget \
        ca-certificates \
        libjpeg-dev \
        libpng-dev \
        python \
        python-dev \
        gfortran \
        libatlas-base-dev \
        libxml2-dev \
        zlib1g-dev \
        libcairo2-dev \
        libeigen3-dev \
        libpcre3-dev \
        openssh-client \
        openssh-server \
        sqlite3 \
        libsqlite3-dev \
        libboost-dev \
        libboost-iostreams-dev \
        libboost-system-dev \
        libboost-thread-dev \
        libboost-serialization-dev \
        libboost-python-dev \
        libboost-regex-dev \
        libglib2.0-dev \
        libopenjp2-7-dev \
        libtiff-dev \
        libgdk-pixbuf2.0-dev \
        openslide-tools \
        libopenslide-dev \
        liblzma-dev \
        google-perftools \
        libgoogle-perftools-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN ln -s $(which python${PYTHON_VERSION}) /usr/local/bin/python

# Install swig
RUN mkdir /tmp/swig && cd /tmp/swig && \
    wget https://downloads.sourceforge.net/project/swig/swig/swig-3.0.12/swig-3.0.12.tar.gz && \
    tar xzf swig-3.0.12.tar.gz && \
    cd swig-3.0.12 && \
    ./configure --prefix=/usr && \
    make -j $(nproc) && \
    make install && \
    rm -rf /tmp/swig

# Install pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python$PYTHON_VERSION get-pip.py && \
    rm get-pip.py && \
    $PIP --no-cache-dir install --upgrade pip && \
    $PIP --no-cache-dir install --upgrade setuptools

# Install python numerics
RUN $PIP install --no-cache-dir h5py==$H5PY_VERSION pyarrow==$PYARROW_VERSION numpy==$NUMPY_VERSION scipy==$SCIPY_VERSION pandas==$PANDAS_VERSION

# Install OpenBabel
RUN mkdir /tmp/openbabel && \
    cd /tmp/openbabel && \
    wget https://github.com/openbabel/openbabel/archive/openbabel-$OPENBABEL_VERSION.tar.gz && \
    tar zxf openbabel-$OPENBABEL_VERSION.tar.gz && \
    mkdir openbabel.build && \
    cd openbabel.build && \
    cmake ../openbabel-openbabel-$OPENBABEL_VERSION -DPYTHON_EXECUTABLE=$(readlink -f `which python`) -DPYTHON_BINDINGS=ON -DRUN_SWIG=ON && \
    make -j $(nproc) && make install && \
    rm -rf /tmp/openbabel

# Install TensorFlow, Keras, and supporting libraries
RUN $PIP install --no-cache-dir tensorflow==$TF_VERSION keras==$KERAS_VERSION
RUN $PIP --no-cache-dir install graph_nets dm-sonnet==$SONNET_VERSION semantic-version contextlib2 tensorflow-probability

# Install additional tools
RUN $PIP install --no-cache-dir tess psutil ppca GPUtil sklearn skll openopt FuncDesigner statsmodels networkx==$NETWORKX_VERSION
RUN $PIP install --no-cache-dir confargparse argcomplete click dill ujson Pyro4==4.23 PyroMP scons pyscons
RUN $PIP install --no-cache-dir Pillow openslide-python scikit-image
RUN $PIP install --no-cache-dir requests boto3
RUN $PIP install --no-cache-dir backports.tempfile backports.lzma
RUN $PIP install --no-cache-dir IPython jupyter
RUN $PIP install --no-cache-dir gast==0.2.2

# Install tflon
RUN mkdir /tmp/tflon && \
    cd /tmp/tflon && hg clone https://bitbucket.org/mkmatlock/tflon && \
    cd tflon && pip install --no-dependencies --no-cache-dir . && \
    rm -rf /tmp/tflon

RUN mkdir /scratch
WORKDIR /scratch
