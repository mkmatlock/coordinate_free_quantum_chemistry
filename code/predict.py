from .train import drop_columns
import os, sys
import pandas as pd
import numpy as np
import click
import tflon
import tensorflow as tf

def compute_average(args):
    if type(args[0].iloc[0,0]) is list:
        return (sum(a.applymap(np.array) for a in args)/float(len(args))).applymap(lambda v: v.tolist())
    else:
        return sum(args)/float(len(args))

@click.command()
@click.option('-p', '--parameter', multiple=True)
@click.option('--filter', default=None)
@click.option('--batch-size', default=50)
@click.option('--data', default='data/test/holdout.tar.gz')
@click.option('--average', default=1, type=int)
@click.argument('modelfile', nargs=1)
@click.argument('outfiles', nargs=-1)
def main(parameter, filter, batch_size, data, average, modelfile, outfiles):
    xargs = eval("{%s}" % (', '.join(parameter)))
    model = tflon.model.Model.load(modelfile, **xargs)

    outfile_d = {os.path.split(fpath)[-1].split('.')[0]:fpath for fpath in outfiles}
    test = tflon.data.TableFeed(model.schema.required.load(data), master_table='molecules')

    if filter is not None:
        with open(filter, 'r') as idxfile:
            idx_filter = {float(line.strip()) for line in idxfile}
        drop_set = set(test.index) - idx_filter
        if len(drop_set)>0:
            test.drop(drop_set)

    with tf.Session():
        model.initialize()

        outputs = {k:[] for k in model.outputs.keys() if k in ['molecule', 'atom', 'bond']}
        for batch in test.iterate(batch_size):
            outp = {k:[] for k in outputs}
            for _ in xrange(average):
                for k,v in model.infer(batch, query=outputs.keys()).items():
                    outp[k].append(v)
            for k in outputs:
                outputs[k].append(compute_average(outp[k]))

    for k in outfile_d:
        df = pd.concat(outputs[k], axis=0)
        tflon.data.write_parquet(df, outfile_d[k])

if __name__=='__main__':
    main()
