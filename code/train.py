import os, sys
import tflon
import tensorflow as tf
import numpy as np
import click

def drop_columns(model, tables):
    if 'atom_qc_properties' in tables:
        drop_cols = set(tables['atom_qc_properties'].columns) - set(model.atom_targets)
        if len(drop_cols)>0:
            tables['atom_qc_properties'].drop(drop_cols, axis=1)

    if 'bond_qc_properties' in tables:
        drop_cols = set(tables['bond_qc_properties'].columns) - set(model.bond_targets)
        if len(drop_cols)>0:
            tables['bond_qc_properties'].drop(drop_cols, axis=1)

    if 'molecule_qc_properties' in tables:
        mt = set(model.mol_targets)

        if mt=={'GapEnergy'}:
            dt = tables['molecule_qc_properties'].data
            dt['GapEnergy'] = dt['eLUMO(Ha)'] - dt['eHOMO(Ha)']

        drop_cols = set(tables['molecule_qc_properties'].columns) - mt
        if len(drop_cols)>0:
            tables['molecule_qc_properties'].drop(drop_cols, axis=1)

        df = tables['molecule_qc_properties'].data
        if 'Dipole(Debye)' in model.mol_targets:
            df['Dipole(Debye)'] = np.arcsinh(df['Dipole(Debye)'])

        oscillator_columns = [c for c in df.columns if c.startswith('OscillatorStrength')]
        if len(oscillator_columns)>0:
            df[oscillator_columns] = np.arcsinh(df[oscillator_columns])

@click.command()
@click.option('--holdout')
@click.option('--distributed', default=False, is_flag=True)
@click.option('--boost', default=False, is_flag=True)
@click.option('--iterations', default=50000)
@click.option('--batch-size', default=64)
@click.option('--prefeaturize', is_flag=True, default=False)
@click.option('--processes', default=3)
@click.option('--data', default='data/train')
@click.option('--decay/--no-decay', default=True)
@click.option('--decay-rate', default=0.98)
@click.argument('templatefile')
@click.argument('modelfile')
@click.argument('workingdir')
def main(holdout, distributed, boost, iterations, batch_size, prefeaturize, processes, data, decay, decay_rate, templatefile, modelfile, workingdir):
    lf = max(iterations/100, 1)
    graph = tf.Graph()
    tflon.data.TensorQueue.DEFAULT_TIMEOUT=10000
    config = tflon.system.configure_resources(reserved_cpus=1)

    local_batch_size = batch_size
    if distributed:
        local_batch_size /= tflon.distributed.num_processes()

    with graph.as_default():
        model = tflon.model.Model.load(templatefile)
        if distributed:
            dataset = tflon.distributed.make_distributed_table_feed(data, model.schema, shard_format='archive')
        else:
            dataset = tflon.data.make_table_feed(data, model.schema, master_table='molecules', shard_format='archive')

        drop_columns(model, dataset)

        if holdout:
            holdouts = {int(line.strip()) for line in open(holdout, 'r')}
            dataset.holdout(holdouts)

        if prefeaturize:
            model.featurize(dataset)

        if decay:
            learning_rate = tf.train.exponential_decay(1e-3, tf.train.get_global_step(), iterations/100.0, decay_rate, staircase=True)
        else:
            learning_rate = 1e-3
        opt = tflon.train.GradientClippingOptimizer(tf.train.AdamOptimizer(learning_rate), 10.0)
        checkpointdir = workingdir + "/checkpoints"
        logdir = workingdir + "/logs"

        if distributed:
            if tflon.distributed.is_master():
                checkpointdir = workingdir + "/checkpoints"
                logdir = workingdir + "/logs"

                trainer = tflon.distributed.DistributedTrainer(opt, iterations, log_frequency=lf, checkpoint=(checkpointdir, 1000), resume=True)
                hook = tflon.train.SummaryHook(logdir, frequency=100, summarize_losses=True, summarize_resources=True, learning_rate=learning_rate)
                trainer.add_hook(hook)
            else:
                trainer = tflon.distributed.DistributedTrainer(opt, iterations, log_frequency=lf)
        else:
            trainer = tflon.train.TFTrainer(opt, iterations, log_frequency=lf, checkpoint=(checkpointdir, 1000), resume=True)
            hook = tflon.train.SummaryHook(logdir, frequency=100, summarize_losses=True, summarize_resources=True, learning_rate=learning_rate)
            trainer.add_hook(hook)


    with tf.Session(graph=graph, config=config):
        if boost:
            v = trainer.add_view('raw_losses')
            booster = tflon.train.BoostGradient(dataset, v)
            generator = booster.shuffle(batch_size=local_batch_size)
        else:
            generator = dataset.shuffle(batch_size=local_batch_size)
        model.fit( generator, trainer, source_tables=dataset, processes=processes, tf_queue_args=dict(capacity=10), coordinator_args=dict(limit=10) if processes>0 else dict() )

        if tflon.distributed.is_master() or not distributed:
            model.save( modelfile )

if __name__=='__main__':
    main()
