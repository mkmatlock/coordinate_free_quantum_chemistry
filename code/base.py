import tflon
import tensorflow as tf
import pandas as pd
from StringIO import StringIO

atom_energies = pd.read_csv(StringIO("""atomicnum	energy
1	-0.5002727762
2	-2.9070489726
3	-7.4909846929
4	-14.6684425428
5	-24.5215179139
6	-37.6739210343
7	-54.5844893657
8	-75.0606214015
9	-99.7155354215
10	-128.8943598845
11	-162.2798809417
12	-200.0793588477
13	-242.2292428899
14	-289.2069235037
15	-341.2580898032
16	-398.1049925383
17	-460.1362417086
18	-527.5171393754
19	-599.8912294586
20	-677.5389803866"""), sep='\t', index_col='atomicnum').energy.to_dict()

class AtomicEnergy(tflon.toolkit.Module):
    def build(self):
        self._atomic_energy = self.add_input('atomic_energy', shape=[None, 1], ttype=tflon.data.Table, dtype=tf.float32)

    def call(self):
        return self._atomic_energy

    def _featurizer(self):
        return AtomicEnergyFeaturizer(self)

class AtomicEnergyFeaturizer(tflon.toolkit.Featurizer):
    def _featurize(self, batch):
        mdf = batch['molecules'].data
        s_col = mdf.columns[0]
        atomic_e = mdf[s_col].apply(lambda mol: sum(atom_energies[an] for an in mol.nodes.atomicnum))
        return {'atomic_energy': atomic_e.to_frame()}

class AtomCount(tflon.toolkit.Module):
    def build(self):
        self._num_atoms = self.add_input('atom_count', shape=[None, 1], ttype=tflon.data.Table, dtype=tf.float32)

    def call(self):
        return self._num_atoms

    def _featurizer(self):
        return AtomCountFeaturizer(self)

class AtomCountFeaturizer(tflon.toolkit.Featurizer):
    def _featurize(self, batch):
        mdf = batch['molecules'].data
        s_col = mdf.columns[0]
        num_atoms = mdf[s_col].apply(lambda mol: len(mol.nodes))
        return {'atom_count': num_atoms.to_frame()}

class Base(tflon.model.Model):
    def _model(self):
        molecules = self.add_table('molecules', ttype=tflon.chem.MoleculeTable)
        atoms = tflon.chem.AtomProperties(molecules, self.atom_types)()
        bonds = None
        if self.use_bonds:
            bonds = tflon.chem.BondProperties(molecules, include_aromatic=self.use_aromatic)()

        neighbor_net = tflon.graph.Neighborhood(molecules, depth=3) | tflon.toolkit.Dense(self.state_size, activation=self.activation)
        atoms = neighbor_net(atoms)

        atom_features = self._calculate_atom_features(molecules, atoms, bonds)
        self._outputs(molecules, atom_features)

    def _calculate_atom_features(self, molecules, atoms):
        raise NotImplementedError()

    def _outputs(self, molecules, atom_features):
        nat = len(self.atom_targets)
        nbt = len(self.bond_targets)
        nmt = len(self.mol_targets)

        if nat > 0:
            atom_targets = self.add_target('atom_qc_properties', shape=[None, nat], ttype=tflon.data.DenseNestedTable)
            atom_norm = tflon.toolkit.NormalizedInput()
            atom_network = tflon.toolkit.Dense(self.state_size/2, activation=self.activation) |\
                        tflon.toolkit.Dense(self.state_size/4, activation=self.activation) |\
                        tflon.toolkit.Dense(nat)
            atom_out = atom_network(atom_features)
            self.add_loss('atom', tflon.toolkit.square_error(atom_norm(atom_targets), atom_out, nans=self.handle_na))
            self.add_output('atom', atom_norm.inverse(atom_out), tflon.graph.DenseNodesToTableTransform(molecules, columns=self.atom_targets))

        if nbt > 0:
            bond_targets = self.add_target('bond_qc_properties', shape=[None, nbt], ttype=tflon.data.DenseNestedTable)
            bond_norm = tflon.toolkit.NormalizedInput()
            bond_network = tflon.graph.NodeToEdge(molecules, width=self.state_size/2, activation=self.activation) |\
                        tflon.toolkit.Dense(self.state_size/4, activation=self.activation) |\
                        tflon.toolkit.Dense(nbt)
            bond_out = bond_network(atom_features)
            self.add_loss('bond', tflon.toolkit.square_error(bond_norm(bond_targets), bond_out, nans=self.handle_na))
            self.add_output('bond', bond_norm.inverse(bond_out), tflon.graph.DenseEdgesToTableTransform(molecules, columns=self.bond_targets))

        if nmt > 0:
            mol_targets = self.add_target('molecule_qc_properties', shape=[None, nmt])
            mol_adjust = AtomCount()() if self.normalize_to_atom_count else tf.constant(1., dtype=tf.float32)
            mol_norm = tflon.toolkit.NormalizedInput('molecule_qc_properties')
            mol_network = tflon.graph.Encoder(molecules,
                                              tf.contrib.rnn.LSTMBlockCell(self.state_size),
                                              tflon.toolkit.Dense(self.state_size, activation=self.activation),
                                              iterations=self.encoder_iters) |\
                        tflon.toolkit.Dense(self.state_size/4) |\
                        tflon.toolkit.Dense(nmt)
            mol_out = mol_network(atom_features)
            normed_targets = mol_norm(mol_targets / mol_adjust)
            denormalized_outputs = mol_norm.inverse(mol_out) * mol_adjust

            if 'TotalE(Ha)' in self.mol_targets:
                i = self.mol_targets.index('TotalE(Ha)')
                total_e = mol_targets[:,i:i+1]
                atomic_e = AtomicEnergy()()

                normed_atomization_e = ((total_e - atomic_e) / mol_adjust - self.atomization_mu) / self.atomization_sigma
                normed_targets = tf.concat([normed_targets[:,:i], normed_atomization_e, normed_targets[:,i+1:]], axis=1)

                normed_atomization_e_output = mol_out[:,i:i+1]
                denormed_total_e_output = (normed_atomization_e_output * self.atomization_sigma + self.atomization_mu) * mol_adjust + atomic_e
                denormalized_outputs = tf.concat([denormalized_outputs[:,:i], denormed_total_e_output, denormalized_outputs[:,i+1:]], axis=1)

            if 'Dipole(Debye)' in self.mol_targets:
                i = self.mol_targets.index('Dipole(Debye)')
                dipole_out = tf.sinh(denormalized_outputs[:,i:i+1])
                denormalized_outputs = tf.concat([denormalized_outputs[:,:i], dipole_out, denormalized_outputs[:,i+1:]], axis=1)

            osc_columns = [c for c in self.mol_targets if c.startswith('OscillatorStrength')]
            if len(osc_columns)>0:
                indices = [self.mol_targets.index(c) for c in osc_columns]
                min_i = min(indices)
                max_i = max(indices)

                osc_out = tf.sinh(denormalized_outputs[:,min_i:max_i+1])
                denormalized_outputs = tf.concat([denormalized_outputs[:,:min_i], osc_out, denormalized_outputs[:,max_i+1:]], axis=1)

            if self.loss_domain=='normalized':
                raw_loss = tflon.toolkit.square_error(normed_targets, mol_out, nans=self.handle_na, reduction=None)
            elif self.loss_domain=='unscaled':
                raw_loss = tflon.toolkit.square_error(mol_targets, denormalized_outputs, nans=self.handle_na, reduction=None)
            else:
                raise ValueError("Loss domain option '%s' not supported" % (self.loss_domain))
            self.add_output('molecule', denormalized_outputs, tflon.data.CopyIndex(molecules, columns=self.mol_targets))
            self.add_output('raw_losses', tf.reduce_sum(raw_loss, axis=1))
            self.add_loss('molecule', tf.reduce_sum(raw_loss))


    def _parameters(self):
        return {'atom_types': 'H B C N O F Si P S Cl As Se Br'.split(),
                'use_bonds': False,
                'use_aromatic': False,
                'atom_targets': [],
                'bond_targets': [],
                'mol_targets':  [],
                'normalize_to_atom_count': False,
                'atomization_mu': -6.130496,
                'atomization_sigma': 1.7611282,
                'passthrough': False,
                'handle_na': False,
                'passes': 5,
                'state_size': 128,
                'activation': tf.nn.elu,
                'encoder_iters': 5,
                'loss_domain': 'normalized'}

# possible atom targets:  'mulliken_charge', 'mulliken_population', 'total_valence', 'bonded_valence', 'NucleophilicDelocalizability', 'ElectrophilicDelocalizability', 'SelfPolarizability'
# possible bond targets:  'bond_order', 'bond_length'
# possible mol targets:   'TotalE(Ha)', 'eHOMO-1(Ha)', 'eHOMO(Ha)', 'eLUMO(Ha)', 'eLUMO+1(Ha)', 'Dipole(Debye)'
# addition mol targets:   'ExcitationGap_n', 'TransitionDipole_n', 'OscillatorStrength_n' for n in 1 .. 9
