from code.base import Base
import tflon
import tensorflow as tf
import graph_nets as gn
import sonnet as snt

def make_mlp_model(latent_size, num_layers):
    return snt.Sequential([
        snt.nets.MLP([latent_size] * num_layers, activate_final=True),
        snt.LayerNorm()
    ])

class MLPGraphIndependent(snt.AbstractModule):
    """GraphIndependent with MLP edge, node, and global models."""

    def __init__(self, latent_size, num_layers, use_globals, name="MLPGraphIndependent"):
        super(MLPGraphIndependent, self).__init__(name=name)
        with self._enter_variable_scope():
            gen_net = lambda: make_mlp_model(latent_size, num_layers)
            self._network = gn.modules.GraphIndependent(node_model_fn=gen_net, edge_model_fn=gen_net, global_model_fn=gen_net if use_globals else None)

    def _build(self, inputs):
        return self._network(inputs)

class GRUNetwork(snt.AbstractModule):
    def __init__(self, latent_size, use_globals, passthrough, activation, name='GRUNetwork'):
        super(GRUNetwork, self).__init__(name=name)
        self._ncell = tf.nn.rnn_cell.GRUCell(latent_size, bias_initializer=tf.constant_initializer(-1.))
        if passthrough or use_globals:
            self._message_net = tflon.toolkit.Dense(latent_size, activation=activation)
            self._state_net = tflon.toolkit.Dense(latent_size, activation=activation)
        self._latent_size = latent_size
        self._use_globals = use_globals
        self._passthrough = passthrough

    def _build(self, inputs):
        lsz = self._latent_size

        if self._passthrough:
            input_tensors = [inputs[:,:2*lsz]]
            state = inputs[:,2*lsz:4*lsz]
            if self._use_globals:
                input_tensors.append( inputs[:,4*lsz:] )
        else:
            input_tensors = [inputs[:,:lsz]]
            state = inputs[:,lsz:2*lsz]
            if self._use_globals:
                input_tensors.append( inputs[:,2*lsz:] )

        used_shape = (sum(i.get_shape()[-1].value for i in input_tensors) + state.get_shape()[-1].value)
        avail_shape = inputs.get_shape()[-1].value
        assert used_shape == avail_shape, "used %d of %d" % (used_shape, avail_shape)
        inputs = tf.concat(input_tensors, axis=-1)

        if state.get_shape()[-1].value > self._latent_size:
            state = self._state_net(state)
        if inputs.get_shape()[-1].value > self._latent_size:
            inputs = self._message_net(inputs)

        out, _ = self._ncell(inputs, state)
        return out

class GraphNetwork(snt.AbstractModule):
    def __init__(self,
                 fn,
                 reducer=tf.unsorted_segment_sum,
                 update_edges=True,
                 update_global=True,
                 edge_block_opt=None,
                 node_block_opt=None,
                 global_block_opt=None,
                 name="graph_network"):
        super(GraphNetwork, self).__init__(name=name)
        self.update_global = update_global
        self.update_edges = update_edges
        if update_edges:
            edge_block_opt = gn.modules._make_default_edge_block_opt(edge_block_opt)
        if update_global:
            global_block_opt = gn.modules._make_default_global_block_opt(global_block_opt, reducer)
        node_block_opt = gn.modules._make_default_node_block_opt(node_block_opt, reducer)

        if not update_global:
            if global_block_opt is None: global_block_opt = {}
            global_block_opt['use_globals'] = False
            if edge_block_opt is None: edge_block_opt = {}
            edge_block_opt['use_globals'] = False
            if node_block_opt is None: node_block_opt = {}
            node_block_opt['use_globals'] = False

        with self._enter_variable_scope():
            if update_edges:
                self._edge_block = gn.blocks.EdgeBlock(edge_model_fn=fn, **edge_block_opt)
            if update_global:
                self._global_block = gn.blocks.GlobalBlock(global_model_fn=fn, **global_block_opt)
            self._node_block = gn.blocks.NodeBlock(node_model_fn=fn, **node_block_opt)

    def _build(self, graph):
        if self.update_edges:
            graph = self._edge_block(graph)
        graph = self._node_block(graph)
        if self.update_global:
            graph = self._global_block(graph)
        return graph

class MLPNodeOutput(snt.AbstractModule):
    """GraphNetwork with MLP node"""

    def __init__(self, latent_size, num_layers, use_globals, name="MLPNodeOutput"):
        super(MLPNodeOutput, self).__init__(name=name)
        with self._enter_variable_scope():
            gen_net = lambda: make_mlp_model(latent_size, num_layers)
            self._network = gn.modules.GraphIndependent(gen_net, lambda: lambda x: x, lambda: lambda x: x if use_globals else None)

    def _build(self, inputs):
        return self._network(inputs)

class MPNN(Base):
    def _calculate_atom_features(self, molecules, atoms, bonds):
        GT = tflon.graph.GraphToGraphsTuple(molecules, nodes=atoms)
        gtuple = GT()
        if bonds is None:
            bonds = GT.zero_edge_state(self.state_size)
        else:
            bonds = tflon.toolkit.Dense(self.state_size, activation=self.activation)(bonds)
        gtuple = gtuple.replace(edges=bonds, globals=GT.zero_global_state(self.state_size))

        for _ in xrange(self.layers):
            encoder = MLPGraphIndependent(self.state_size, 1, self.use_globals)
            if self.update_net == 'MLP':
                gen_net = lambda: make_mlp_model(self.state_size, 1)
            elif self.update_net == 'GRU':
                gen_net = lambda: GRUNetwork(self.state_size, self.use_globals, self.passthrough, self.activation)
            else:
                raise ValueError("Unsupported update network '%s'" % (self.update_net))
            core = GraphNetwork(gen_net, update_global=self.use_globals, update_edges=self.update_edges)
            decoder = MLPNodeOutput(self.state_size, 1, self.use_globals)

            latent = encoder(gtuple)
            latent0 = latent
            for _ in xrange(self.passes):
                core_input = latent
                if self.passthrough:
                    core_input = gn.utils_tf.concat([latent0, core_input], axis=1)
                latent = core(core_input)
                if not self.update_edges or not self.use_globals:
                    latent = gn.graphs.GraphsTuple(nodes=latent.nodes, edges=latent0.edges, globals=latent0.globals,
                                                   senders=latent.senders, receivers=latent.receivers,
                                                   n_node=latent.n_node, n_edge=latent.n_edge)


            gtuple = decoder(latent)

        return gtuple.nodes

    def _parameters(self):
        params = super(MPNN, self)._parameters()
        params['use_globals'] = True         # Use and update global properties
        params['update_edges'] = True           # Update edge properties
        params['update_net'] = 'MLP'    # 'GRU' or 'MLP'
        params['layers'] = 1
        return params

if __name__=='__main__':
    model = MPNN(use_globals=False, update_edges=False, update_net='GRU', passthrough=True)
    print len(model)
