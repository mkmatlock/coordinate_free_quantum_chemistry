import tflon
import tensorflow as tf
from code.base import Base

class Wave(Base):
    def _calculate_atom_features(self, molecules, atoms, bonds):
        if self.centroids=='cfc_centrality':
            rooter = lambda g: g.centroids(scorer=tflon.graph.center.cfc_centrality_scorer, selector=tflon.graph.center.select_random_top_epsilon)
        elif self.centroids=='eccentricity':
            rooter = lambda g: g.centroids(selector=tflon.graph.center.select_softmin)
        else:
            raise ValueError("Unrecognized value for option 'centroids': %s" % (self.centroids))
        grnn = tflon.graph.GRNN(molecules, rooter=rooter, edge_properties=bonds is not None)

        def cell_factory():
            if self.cell_factory == 'GRU':
                ncell = tf.nn.rnn_cell.GRUCell(self.state_size, bias_initializer=tf.constant_initializer(-1.))
            elif self.cell_factory == 'MLP':
                ncell = tf.nn.rnn_cell.BasicRNNCell(self.state_size, activation=tf.tanh)
            elif self.cell_factory == 'MLP_ELU':
                ncell = tf.nn.rnn_cell.BasicRNNCell(self.state_size, activation=tf.nn.elu)
            else:
                raise ValueError("Invalid cell_factory: '%s'" % (self.cell_factory))
            return grnn.wrap_cell(ncell, normalization='layer', normalize_output=True, gate_types=self.gate_types)

        if self.dynamic:
            fcell, bcell = cell_factory(), cell_factory()
            dyn = tflon.graph.DynamicPasses(molecules, feature='radius', max_passes=self.passes)
            net = tflon.toolkit.ForLoop(lambda _,x: grnn(x, forward_cell=fcell, backward_cell=bcell, node_properties=atoms if self.passthrough else None, edge_properties=bonds), iterations=dyn.batch_max_passes, collect_outputs=True) |\
                  dyn
        elif self.reuse:
            fcell, bcell = cell_factory(), cell_factory()
            net = tflon.toolkit.Chain(*[lambda x: grnn(x, forward_cell=fcell, backward_cell=bcell, node_properties=atoms if self.passthrough else None, edge_properties=bonds)]*self.passes)
        else:
            net = tflon.toolkit.Chain(*[lambda x: grnn(x, forward_cell=cell_factory(), backward_cell=cell_factory(), node_properties=atoms if self.passthrough else None, edge_properties=bonds)]*self.passes)
        return net(atoms)

    def _parameters(self):
        params = super(Wave, self)._parameters()
        params['cell_factory'] = 'GRU'
        params['reuse'] = True
        params['dynamic'] = False
        params['centroids'] = 'eccentricity'
        params['gate_types'] = ['softsign', 'softmax']
        return params

if __name__=='__main__':
    model = Wave(gate_types=['sum'])
    print len(model)
