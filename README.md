# Sample code for "Deep learning coordinate-free quantum chemistry"

Contents of this package include:
* A docker recipe to set up required software
* Code to build and train models
* Demo code showing how to generate total energy predictions on new
  molecules
* Complete training and test data used in the paper

## Docker

We have provided a Dockerfile with recipes to install all dependencies
including tensorflow, tflon and graph_nets. The latter two are required to run
experiments with the Wave and MPNN architectures, respectively. The last
successful build of this docker container was performed on December
31st, 2019. The provided docker container does not support GPU
acceleration, but this can easily be added. Build the docker container
with:

```
    docker build -f Dockerfile -t deep_quantum .
```


## Example calculations

We have provided a Jupyter notebook demonstrating calculations of total
energy with the Wave and MPNN-G architectures used in our paper. A few
example molecules are pre-processed and fed to tflon for futher processing and calculation. Using the docker container, this notebook can be run in Jupyter:

```
    docker run -it -v `pwd`:/scratch deep_quantum jupyter notebook --no-browser --ip=0.0.0.0
```

## Training a new model

We have also provided code and data to train new models. Tflon uses
model templates, created from classes inheriting from the
tflon.model.Model class (see documentation at tflon.readthedocs.io).
These templates can then be trained using the included code/train.py
script to
generated a trained copy of the model. Trained models can be used to
predict on large, pre-processed datasets with the code/predict.py
script. the training and test data are stored in data/train and
data/test.tar.gz, respectively

```
# Create the model template from a class extending the tflon API Model base class
tflon model create code.wave wave.Wave "{'mol_targets': ['TotalE(Ha)'], 'normalize_to_atom_count': True, 'gate_types': ['softsign']}" u0_wave.pyp

# Train the model on preprocessed and sharded training data (can be run in distributed mode using MPI)
python -m code.train --data data/train --holdout data/test/holdout.ids u0_wave.pyp u0_wave_trained.pyp logs/

# Generate predictions on holdout test set (averaging over several predictions is only necessary for Wave models)
python -m code.predict --data data/test.tar.gz --average 10 u0_wave_trained.pyp u0_wave_out.pq
```
